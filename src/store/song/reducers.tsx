import { Song } from "./types";

const initialState: Song[] = [
  { id: "1", name: "Pink Floyd: Wish you where here" },
  { id: "2", name: "Pink Floyd: Echoes" }, { id: "3", name: "Radio Head: Street Spirit" },
  { id: "4", name: "Bob Marley: Natural Mystic" },
]

export function songReducer(state = initialState): Song[] {
  return state;
}
