
import { Song } from './../song/types'

export interface Playlist {
  id: string;
  name: string;
  color: string;
  songs: Song[];
}

export const SET_PLAYLIST = "SET_PLAYLIST";

export interface SetPlaylistAction {
  type: typeof SET_PLAYLIST;
  payload: Playlist;
}
