import { Playlist, SET_PLAYLIST } from "./types";

export function setPlaylist(playlist: Playlist) {
  return {
    type: SET_PLAYLIST,
    payload: playlist
  };
}
