import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
// import { composeWithDevTools } from "redux-devtools-extension";

import { playlistReducer } from "./playlist/reducers";
import { libraryReducer } from "./library/reducers";
import { songReducer } from "./song/reducers";

const rootReducer = combineReducers({
  playlistReducer: playlistReducer,
  libraryReducer: libraryReducer,
  songReducer: songReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  applyMiddleware(thunkMiddleware)
);