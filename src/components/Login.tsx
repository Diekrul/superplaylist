import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';

const logo = require("./../../public/images/logo2.png");

interface IProps {
  navigation: NavigationScreenProp<any, any>
}

const Login = (props: IProps) => {
  return (
    <View style={styles.container}>
      <View style={styles.sectionLogo}>
        <Image source={logo} style={styles.logo} resizeMode='cover' />
        <Text style={styles.logoText}>Super PlayList</Text>
      </View>
      <View style={styles.separator}></View>
      <View style={styles.loginFooter}>
        <TouchableOpacity onPress={() => props.navigation.navigate('Main')}
            accessibilityLabel="decrement" style={styles.button}>
          <Text style={styles.buttonText}>ENTER</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Login

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ef6c00"
  },
  sectionLogo: {
    alignItems: 'center',
    margin: 10,
  },
  logo: {
    marginTop: 30,
    height: 250,
    width: 250,
  },
  logoText: {
    marginTop: 20,
    fontSize: 32,
    textAlign: "center",
    color: 'white',
  },
  loginFooter: {
    display: 'flex',
    alignItems: "center",
    textAlign: "center",
    height: 400
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    width: 300,
  },
  buttonText: {
    color:'white',
    fontSize:18,
    fontWeight:'bold'
  },
  separator: {
    height: 200
  }
})
